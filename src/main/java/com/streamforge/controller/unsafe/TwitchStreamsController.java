package com.streamforge.controller.unsafe;

import com.streamforge.controller.common.ApiConstants;
import com.streamforge.service.twitch.streams.TwitchStreamsService;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Api
@RestController
@RequestMapping(ApiConstants.TWITCH_STREAMS_PATH)
@CrossOrigin
public class TwitchStreamsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TwitchStreamsController.class);

    @Autowired
    private TwitchStreamsService twitchStreamsService;

    @RequestMapping(path = "/external", method = RequestMethod.GET)
    public ResponseEntity<?> getStreams() {
        try {
            return new ResponseEntity<>(twitchStreamsService.getStreamsData(), null, HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error("Attempt to fetch streams failed", e);
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
    }

}
