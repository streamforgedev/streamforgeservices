package com.streamforge.controller.unsafe;

import com.streamforge.controller.common.ApiConstants;
import com.streamforge.data.dto.widgets.WidgetComplexDto;
import com.streamforge.data.dto.widgets.WidgetDto;
import com.streamforge.service.common.security.ExternalSessionService;
import com.streamforge.service.common.security.widget.ExternalWidgetService;
import com.streamforge.service.common.security.widget.WidgetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(ApiConstants.UNSAFE_WIDGET_PATH)
@CrossOrigin(origins = {"http://localhost:8080", "http://localhost:7001"})
public class ExternalWidgetController {

    @Autowired
    private ExternalWidgetService externalWidgetService;

    @GetMapping("/{userId}/token/{widgetToken}")
    public ResponseEntity<WidgetComplexDto> getWidgetByToken(@PathVariable(value = "userId") Long userId,
                                                             @PathVariable(value = "widgetToken") String widgetToken) {

        return ResponseEntity.ok(externalWidgetService.getWidgetData(userId, widgetToken));
    }

}
