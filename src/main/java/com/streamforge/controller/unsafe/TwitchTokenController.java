package com.streamforge.controller.unsafe;

import com.streamforge.controller.common.ApiConstants;
import com.streamforge.data.dto.TwitchTokenDto;
import com.streamforge.service.common.Constants;
import com.streamforge.service.common.security.ExternalSessionService;
import com.streamforge.service.twitch.TwitchTokenService;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;

@Api
@RestController
@RequestMapping(ApiConstants.TOKEN_PATH)
public class TwitchTokenController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TwitchTokenController.class);

    private final TwitchTokenService twitchTokenService;
    private final ExternalSessionService externalSessionService;

    @Autowired
    public TwitchTokenController(TwitchTokenService twitchTokenService,
                                 ExternalSessionService externalSessionService) {
        this.twitchTokenService = twitchTokenService;
        this.externalSessionService = externalSessionService;
    }

    @PostMapping("external/save")
    public ResponseEntity<?> saveTwitchToken(@RequestBody TwitchTokenDto tokenDto) {
        String token = Constants.TOKEN_CUSTOM  + Constants.WHITESPACE + twitchTokenService.saveOrUpdateTwitchSession(tokenDto);

        HttpHeaders headers = new HttpHeaders();
        headers.put(ApiConstants.AUTHORIZATION, Collections.singletonList(token));
        return ResponseEntity.ok().headers(headers).build();
    }

    @PostMapping("external/refresh")
    public ResponseEntity<?> validateTwitchToken(@RequestHeader(name = ApiConstants.AUTHORIZATION) String token) {
        String tokenBody = token.split(Constants.WHITESPACE)[1];
        String refreshedToken = externalSessionService.refreshServiceToken(tokenBody);

        HttpHeaders headers = new HttpHeaders();
        headers.put(ApiConstants.AUTHORIZATION, Collections.singletonList(refreshedToken));
        return ResponseEntity.ok().headers(headers).build();
    }

}
