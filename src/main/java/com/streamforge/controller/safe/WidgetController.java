package com.streamforge.controller.safe;

import com.streamforge.controller.common.ApiConstants;
import com.streamforge.data.dto.widgets.WidgetDto;
import com.streamforge.data.entity.ExternalSession;
import com.streamforge.service.common.security.ExternalSessionService;
import com.streamforge.service.common.security.widget.WidgetService;
import com.streamforge.service.exception.UserAuthenticationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(ApiConstants.WIDGET_PATH)
@CrossOrigin(origins = {"http://localhost:8080", "http://localhost:7001"})
public class WidgetController {

    @Autowired
    private ExternalSessionService externalSessionService;

    @Autowired
    private WidgetService widgetService;

    @PostMapping()
    public ResponseEntity createWidget(@RequestHeader(name = "Authorization") String token,
                                       @RequestBody WidgetDto widgetDto) {
        ExternalSession session = externalSessionService.getExternalSession(token)
                .orElseThrow(() -> new UserAuthenticationException(""));

        return ResponseEntity.ok(widgetService.createWidget(session.getAccessToken(), widgetDto,
                externalSessionService.identifyWebUserId(token)));
    }

    @GetMapping("/all")
    public ResponseEntity<List<WidgetDto>> getWidgets(@RequestHeader(name = "Authorization") String token) {
        return ResponseEntity.ok(widgetService.getWidgets(externalSessionService.identifyWebUserId(token)));
    }

    @GetMapping("/status/{widgetId}")
    public ResponseEntity<WidgetDto> getWidget(@RequestHeader(name = "Authorization") String token,
                                               @PathVariable(value = "widgetId") Long widgetId) {
        return ResponseEntity.ok(widgetService.getWidget(externalSessionService.identifyWebUserId(token), widgetId));
    }

    @DeleteMapping("/status/{widgetId}")
    public ResponseEntity deleteWidget(@RequestHeader(name = "Authorization") String token,
                                       @PathVariable(value = "widgetId") Long widgetId) {
        widgetService.removeWidget(widgetId);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/status/token/{widgetToken}")
    public ResponseEntity<WidgetDto> getWidgetByToken(@RequestHeader(name = "Authorization") String token,
                                                      @PathVariable(value = "widgetToken") String widgetToken) {
        return ResponseEntity.ok(widgetService.getWidget(externalSessionService.identifyWebUserId(token), widgetToken));
    }

}
