package com.streamforge.controller.safe;

import com.streamforge.controller.common.ApiConstants;
import com.streamforge.data.dto.UserDataDto;
import com.streamforge.data.dto.twitch.TwitchUserDto;
import com.streamforge.data.dto.twitch.UserDataNativeDto;
import com.streamforge.data.dto.twitch.containers.TwitchBitsListDto;
import com.streamforge.data.dto.twitch.containers.TwitchComplexDto;
import com.streamforge.data.dto.twitch.containers.TwitchFollowersListDto;
import com.streamforge.data.dto.twitch.containers.TwitchStreamListDto;
import com.streamforge.data.entity.ExternalSession;
import com.streamforge.data.entity.Session;
import com.streamforge.data.entity.WebUser;
import com.streamforge.data.repository.WebUserRepository;
import com.streamforge.service.common.AuthorizationService;
import com.streamforge.service.common.UserDataService;
import com.streamforge.service.common.security.ExternalSessionService;
import com.streamforge.service.exception.UserAuthenticationException;
import com.streamforge.service.twitch.TwitchUserDataService;
import com.streamforge.service.twitch.streams.TwitchStreamsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(ApiConstants.USER_PATH)
@CrossOrigin(origins = {"http://localhost:8080", "http://localhost:7001"})
public class UserController {

    @Autowired
    private ExternalSessionService externalSessionService;

    @Autowired
    private UserDataService userDataService;

    @Autowired
    private TwitchUserDataService twitchUserDataService;

    @Autowired
    private TwitchStreamsService twitchStreamsService;

    @Autowired
    private AuthorizationService authorizationService;

    @GetMapping()
    public ResponseEntity<UserDataDto> getUserInfo(@RequestHeader(name = "Authorization") String token) {
        Long userId = externalSessionService.identifyWebUserId(token);
        UserDataDto userDataDto = userDataService.processUserData(userId);
        return ResponseEntity.ok(userDataDto);
    }

    @GetMapping("/twitch")
    public ResponseEntity<TwitchUserDto> getTwitchUserInfo(@RequestHeader(name = "Authorization") String token) {
        Long userId = externalSessionService.identifyWebUserId(token);
        UserDataDto userDataDto = userDataService.processUserData(userId);
        return ResponseEntity.ok(userDataService.getTwitchUserData(userDataDto.getExternalUserId()));
    }

    @GetMapping("/bits")
    public ResponseEntity<TwitchBitsListDto> getBitsLeaderboard(@RequestHeader(name = "Authorization") String token) {
        ExternalSession session = externalSessionService.getExternalSession(token)
                .orElseThrow(() -> new UserAuthenticationException(""));

        return ResponseEntity.ok(twitchUserDataService.getBitsLeaderboard(session.getAccessToken()));
    }

    @GetMapping("/stream")
    public ResponseEntity<TwitchStreamListDto> getStream(@RequestHeader(name = "Authorization") String token) {
        Long userId = externalSessionService.identifyWebUserId(token);
        UserDataDto userDataDto = userDataService.processUserData(userId);
        ExternalSession session = externalSessionService.getExternalSession(token)
                .orElseThrow(() -> new UserAuthenticationException(""));

        return ResponseEntity.ok(twitchStreamsService.getUserStream(userDataDto.getExternalUserId(), session.getAccessToken()));
    }

    @GetMapping("/followers")
    public ResponseEntity<TwitchFollowersListDto> getFollowers(@RequestHeader(name = "Authorization") String token) {
        Long userId = externalSessionService.identifyWebUserId(token);
        UserDataDto userDataDto = userDataService.processUserData(userId);
        ExternalSession session = externalSessionService.getExternalSession(token)
                .orElseThrow(() -> new UserAuthenticationException(""));

        return ResponseEntity.ok(twitchStreamsService.getFollowers(userDataDto.getExternalUserId(), session.getAccessToken()));
    }

    @GetMapping("/followed")
    public ResponseEntity<TwitchFollowersListDto> getFollowed(@RequestHeader(name = "Authorization") String token) {
        Long userId = externalSessionService.identifyWebUserId(token);
        UserDataDto userDataDto = userDataService.processUserData(userId);
        ExternalSession session = externalSessionService.getExternalSession(token)
                .orElseThrow(() -> new UserAuthenticationException(""));

        return ResponseEntity.ok(twitchStreamsService.getFollowed(userDataDto.getExternalUserId(), session.getAccessToken()));
    }

    @GetMapping("/twitch/full")
    public ResponseEntity<TwitchComplexDto> getFullData(@RequestHeader(name = "Authorization") String token) {
        Long userId = externalSessionService.identifyWebUserId(token);
        UserDataDto userDataDto = userDataService.processUserData(userId);
        ExternalSession session = externalSessionService.getExternalSession(token)
                .orElseThrow(() -> new UserAuthenticationException(""));

        return ResponseEntity.ok(twitchUserDataService.getFull(userDataDto.getExternalUserId(), session.getAccessToken()));
    }

}
