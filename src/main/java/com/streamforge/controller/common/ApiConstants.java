package com.streamforge.controller.common;

import org.springframework.stereotype.Component;

@Component
public class ApiConstants {

    public static final String UNSAFE_API = "/api";
    public static final String TWITCH_COMMON_PATH = UNSAFE_API + "twitch/common";
    public static final String TOKEN_PATH = UNSAFE_API + "/token";
    public static final String TWITCH_STREAMS_PATH = UNSAFE_API + "/streams";
    public static final String UNSAFE_WIDGET_PATH = UNSAFE_API + "/widget";

    public static final String SAFE_API = "/api/safe";
    public static final String SECURITY_PATH = SAFE_API + "/security";
    public static final String USER_PATH = SAFE_API + "/user";
    public static final String WIDGET_PATH = SAFE_API + "/widget";
    public static final String PUBSUB_PATH = SAFE_API + "/pubsub";


    public static final String AUTHORIZATION = "Authorization";

}
