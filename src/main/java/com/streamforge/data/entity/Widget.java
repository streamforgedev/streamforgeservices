package com.streamforge.data.entity;

import com.streamforge.data.dto.widgets.WidgetType;

import javax.persistence.*;

@NamedQueries({
        @NamedQuery(
                name = Widget.SQL_FIND_BY_TOKEN,
                query = "select w from Widget w where w.widgetToken = :token"
        )
})
@Entity
@Table(name = "widget")
public class Widget {

    public static final String SQL_FIND_BY_TOKEN = "WebUser.findByToken";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "widget_id")
    private Long widgetId;

    @Column(name = "is_active")
    private Boolean active;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private WidgetType type;

    @Column(name = "appearance")
    private String appearance;

    @Column(name = "topic")
    private String topic;

    @Column(name = "widget_token", unique = true)
    private String widgetToken;

    @ManyToOne
    @JoinColumn(name = "userId")
    private WebUser user;

    public Long getWidgetId() {
        return widgetId;
    }

    public void setWidgetId(Long widgetId) {
        this.widgetId = widgetId;
    }

    public WebUser getUser() {
        return user;
    }

    public void setUser(WebUser user) {
        this.user = user;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getWidgetToken() {
        return widgetToken;
    }

    public void setWidgetToken(String widgetToken) {
        this.widgetToken = widgetToken;
    }

    public WidgetType getType() {
        return type;
    }

    public void setType(WidgetType type) {
        this.type = type;
    }

    public String getAppearance() {
        return appearance;
    }

    public void setAppearance(String appearance) {
        this.appearance = appearance;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }
}
