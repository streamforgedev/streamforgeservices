package com.streamforge.data.repository;

import com.streamforge.data.entity.Widget;
import com.streamforge.data.repository.custom.WidgetRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WidgetRepository extends JpaRepository<Widget, Long>, WidgetRepositoryCustom {

}
