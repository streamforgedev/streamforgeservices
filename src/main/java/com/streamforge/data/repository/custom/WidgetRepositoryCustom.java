package com.streamforge.data.repository.custom;

import com.streamforge.data.entity.Widget;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface WidgetRepositoryCustom {

    @Transactional(readOnly = true)
    Optional<Widget> findByToken(String token);

}
