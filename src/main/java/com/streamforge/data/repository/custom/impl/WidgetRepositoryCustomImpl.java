package com.streamforge.data.repository.custom.impl;

import com.streamforge.data.entity.Widget;
import com.streamforge.data.repository.custom.WidgetRepositoryCustom;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

public class WidgetRepositoryCustomImpl implements WidgetRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    @Transactional(readOnly = true)
    public Optional<Widget> findByToken(String token) {
        List<Widget> widgets = entityManager
                .createNamedQuery(Widget.SQL_FIND_BY_TOKEN, Widget.class)
                .setParameter("token", token)
                .getResultList();

        return widgets.isEmpty() ? Optional.empty() : Optional.of(widgets.get(0));
    }

}
