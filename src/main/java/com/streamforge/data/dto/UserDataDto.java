package com.streamforge.data.dto;

public class UserDataDto {

    private long userId;
    private long externalUserId;
    private String name;
    private String displayName;

    public long getUserId() {
        return userId;
    }

    public UserDataDto setUserId(long userId) {
        this.userId = userId;
        return this;
    }

    public long getExternalUserId() {
        return externalUserId;
    }

    public UserDataDto setExternalUserId(long externalUserId) {
        this.externalUserId = externalUserId;
        return this;
    }

    public String getName() {
        return name;
    }

    public UserDataDto setName(String name) {
        this.name = name;
        return this;
    }

    public String getDisplayName() {
        return displayName;
    }

    public UserDataDto setDisplayName(String displayName) {
        this.displayName = displayName;
        return this;
    }

}
