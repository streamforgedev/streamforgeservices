package com.streamforge.data.dto.widgets;

import java.util.ArrayList;
import java.util.List;

public class PubSubPayloadDto {
    private List<String> topics = new ArrayList<>();
    private String auth_token;

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }

    public List<String> getTopics() {
        return topics;
    }

    public void setTopics(List<String> topics) {
        this.topics = topics;
    }
}
