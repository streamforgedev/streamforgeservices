package com.streamforge.data.dto.widgets;

public enum WidgetType {
    CHAT(""),
    BITS("channel-bits-events-v2.%s"),
    SUB("channel-subscribe-events-v1.%s"),
    WHISPERS("whispers.%s");

    private String topic;

    WidgetType(String topic) {
        this.topic = topic;
    }

    public String getTopic() {
        return this.topic;
    }
}
