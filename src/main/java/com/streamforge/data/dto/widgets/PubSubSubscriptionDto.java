package com.streamforge.data.dto.widgets;

import java.util.UUID;

public class PubSubSubscriptionDto {
    private String type;
    private String nonce = UUID.randomUUID().toString();
    private PubSubPayloadDto data;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }

    public PubSubPayloadDto getData() {
        return data;
    }

    public void setData(PubSubPayloadDto data) {
        this.data = data;
    }
}
