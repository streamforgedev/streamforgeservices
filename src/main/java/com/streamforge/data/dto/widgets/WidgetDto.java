package com.streamforge.data.dto.widgets;

public class WidgetDto {

    private String widgetToken;
    private WidgetType type;
    private String appearance;
    private Long id;
    private Boolean isActive;
    private Long userId;
    private Long userExternalId;
    private String topic;
    private String login;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public String getWidgetToken() {
        return widgetToken;
    }

    public void setWidgetToken(String widgetToken) {
        this.widgetToken = widgetToken;
    }

    public Long getUserExternalId() {
        return userExternalId;
    }

    public void setUserExternalId(Long userExternalId) {
        this.userExternalId = userExternalId;
    }

    public WidgetType getType() {
        return type;
    }

    public void setType(WidgetType type) {
        this.type = type;
    }

    public String getAppearance() {
        return appearance;
    }

    public void setAppearance(String appearance) {
        this.appearance = appearance;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
