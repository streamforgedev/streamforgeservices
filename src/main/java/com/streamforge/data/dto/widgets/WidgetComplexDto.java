package com.streamforge.data.dto.widgets;

public class WidgetComplexDto {
    private PubSubSubscriptionDto pubSubSubscriptionDto;
    private WidgetDto widgetDto;

    public WidgetComplexDto(PubSubSubscriptionDto pubSubSubscriptionDto, WidgetDto widgetDto) {
        this.pubSubSubscriptionDto = pubSubSubscriptionDto;
        this.widgetDto = widgetDto;
    }

    public PubSubSubscriptionDto getPubSubSubscriptionDto() {
        return pubSubSubscriptionDto;
    }

    public void setPubSubSubscriptionDto(PubSubSubscriptionDto pubSubSubscriptionDto) {
        this.pubSubSubscriptionDto = pubSubSubscriptionDto;
    }

    public WidgetDto getWidgetDto() {
        return widgetDto;
    }

    public void setWidgetDto(WidgetDto widgetDto) {
        this.widgetDto = widgetDto;
    }
}
