package com.streamforge.data.dto.twitch;

import com.fasterxml.jackson.annotation.JsonAlias;

public class BitsDto {

    @JsonAlias("user_id")
    private String username;

    @JsonAlias("user_name")
    private String viewerCount;

    private Long rank;
    private Long score;

    public BitsDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getViewerCount() {
        return viewerCount;
    }

    public void setViewerCount(String viewerCount) {
        this.viewerCount = viewerCount;
    }

    public Long getRank() {
        return rank;
    }

    public void setRank(Long rank) {
        this.rank = rank;
    }

    public Long getScore() {
        return score;
    }

    public void setScore(Long score) {
        this.score = score;
    }
}
