package com.streamforge.data.dto.twitch.containers;

import com.streamforge.data.dto.twitch.BitsDto;

import java.util.List;

public class TwitchBitsListDto {

    private List<BitsDto> data;

    public List<BitsDto> getData() {
        return data;
    }

    public void setData(List<BitsDto> data) {
        this.data = data;
    }

}
