package com.streamforge.data.dto.twitch.containers;

import com.streamforge.data.dto.twitch.BitsDto;
import com.streamforge.data.dto.twitch.FollowerDto;

import java.util.List;

public class TwitchFollowersListDto {

    private List<FollowerDto> data;
    private Long total;

    public List<FollowerDto> getData() {
        return data;
    }

    public void setData(List<FollowerDto> data) {
        this.data = data;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }
}
