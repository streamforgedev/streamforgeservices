package com.streamforge.data.dto.twitch;

import com.fasterxml.jackson.annotation.JsonAlias;

import java.util.Date;

public class StreamDto {

    @JsonAlias("user_name")
    private String username;

    @JsonAlias("viewer_count")
    private int viewerCount;

    @JsonAlias("started_at")
    private Date startedAt;

    @JsonAlias("thumbnail_url")
    private String thumbnailUrl;

    private String title;
    private String type;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getViewerCount() {
        return viewerCount;
    }

    public void setViewerCount(int viewerCount) {
        this.viewerCount = viewerCount;
    }

    public Date getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(Date startedAt) {
        this.startedAt = startedAt;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl.replace("{width}", "1920")
                .replace("{height}", "1080");
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
