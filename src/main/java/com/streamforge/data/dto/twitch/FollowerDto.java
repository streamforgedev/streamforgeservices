package com.streamforge.data.dto.twitch;

import com.fasterxml.jackson.annotation.JsonAlias;

public class FollowerDto {

    @JsonAlias("from_id")
    private String followerId;

    @JsonAlias("from_name")
    private String followerName;

    @JsonAlias("to_id")
    private String selfId;

    @JsonAlias("to_name")
    private String selfName;

    public FollowerDto() {
    }

    public String getFollowerId() {
        return followerId;
    }

    public void setFollowerId(String followerId) {
        this.followerId = followerId;
    }

    public String getFollowerName() {
        return followerName;
    }

    public void setFollowerName(String followerName) {
        this.followerName = followerName;
    }

    public String getSelfId() {
        return selfId;
    }

    public void setSelfId(String selfId) {
        this.selfId = selfId;
    }

    public String getSelfName() {
        return selfName;
    }

    public void setSelfName(String selfName) {
        this.selfName = selfName;
    }
}
