package com.streamforge.data.dto.twitch.containers;

public class TwitchComplexDto {
    private TwitchBitsListDto bits;
    private TwitchFollowersListDto followers;
    private TwitchFollowersListDto followed;
    private TwitchStreamListDto stream;

    public TwitchBitsListDto getBits() {
        return bits;
    }

    public void setBits(TwitchBitsListDto bits) {
        this.bits = bits;
    }

    public TwitchFollowersListDto getFollowers() {
        return followers;
    }

    public void setFollowers(TwitchFollowersListDto followers) {
        this.followers = followers;
    }

    public TwitchFollowersListDto getFollowed() {
        return followed;
    }

    public void setFollowed(TwitchFollowersListDto followed) {
        this.followed = followed;
    }

    public TwitchStreamListDto getStream() {
        return stream;
    }

    public void setStream(TwitchStreamListDto stream) {
        this.stream = stream;
    }
}
