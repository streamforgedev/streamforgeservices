package com.streamforge.data.dto.twitch;

import com.fasterxml.jackson.annotation.JsonAlias;

import java.util.Date;

public class ChannelDataNativeDto {

    @JsonAlias("_id")
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}

