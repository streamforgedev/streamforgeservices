package com.streamforge.data.dto.twitch;

import com.fasterxml.jackson.annotation.JsonAlias;

public class TwitchUserDto {

    private String login;
    private String type;
    private String description;

    @JsonAlias("display_name")
    private String displayName;

    @JsonAlias("broadcaster_type")
    private String broadcasterType;

    @JsonAlias("profile_image_url")
    private String profileImageUrl;

    @JsonAlias("offline_image_url")
    private String offlineImageUrl;

    @JsonAlias("view_count")
    private long viewCount;

    public TwitchUserDto() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBroadcasterType() {
        return broadcasterType;
    }

    public void setBroadcasterType(String broadcasterType) {
        this.broadcasterType = broadcasterType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public String getOfflineImageUrl() {
        return offlineImageUrl;
    }

    public void setOfflineImageUrl(String offlineImageUrl) {
        this.offlineImageUrl = offlineImageUrl;
    }

    public long getViewCount() {
        return viewCount;
    }

    public void setViewCount(long viewCount) {
        this.viewCount = viewCount;
    }
}
