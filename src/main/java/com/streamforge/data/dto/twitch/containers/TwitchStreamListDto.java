package com.streamforge.data.dto.twitch.containers;

import com.streamforge.data.dto.twitch.StreamDto;
import com.streamforge.data.dto.twitch.TwitchUserDto;

import java.util.List;

public class TwitchStreamListDto {

    private List<StreamDto> data;

    public List<StreamDto> getData() {
        return data;
    }

    public void setData(List<StreamDto> data) {
        this.data = data;
    }

}
