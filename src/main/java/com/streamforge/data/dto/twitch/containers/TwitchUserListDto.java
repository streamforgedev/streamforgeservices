package com.streamforge.data.dto.twitch.containers;

import com.streamforge.data.dto.twitch.TwitchUserDto;

import java.util.List;

public class TwitchUserListDto {

    private List<TwitchUserDto> data;

    public List<TwitchUserDto> getData() {
        return data;
    }

    public void setData(List<TwitchUserDto> data) {
        this.data = data;
    }

}
