package com.streamforge.service.transformer;

import org.springframework.stereotype.Service;

@Service
public abstract class AbstractTransformer<F, T> {

    public T transform(F from) {
        if (validate(from)) {
            return transformInternal(from);
        } else {
            return null;
        }
    }

    protected abstract T transformInternal(F from);

    protected boolean validate(F from) {
        return true;
    }
}
