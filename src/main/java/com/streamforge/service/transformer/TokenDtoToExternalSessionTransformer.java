package com.streamforge.service.transformer;

import com.streamforge.data.dto.TwitchTokenDto;
import com.streamforge.data.entity.ExternalSession;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class TokenDtoToExternalSessionTransformer extends AbstractTransformer<TwitchTokenDto, ExternalSession> {

    @Override
    protected ExternalSession transformInternal(TwitchTokenDto from) {
        Date expirationDate = new DateTime().plusSeconds(from.getExpiration()).toDate();

        return new ExternalSession.ExternalSessionBuilder()
                .setAccessToken(from.getAccessToken())
                .setRefreshToken(from.getRefreshToken())
                .setExpirationDate(expirationDate)
                .build();
    }

}
