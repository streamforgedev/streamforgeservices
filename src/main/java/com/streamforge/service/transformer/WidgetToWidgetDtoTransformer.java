package com.streamforge.service.transformer;

import com.streamforge.data.dto.widgets.WidgetDto;
import com.streamforge.data.entity.Widget;
import org.springframework.stereotype.Service;

@Service
public class WidgetToWidgetDtoTransformer extends AbstractTransformer<Widget, WidgetDto> {

    @Override
    protected WidgetDto transformInternal(Widget from) {
        WidgetDto to = new WidgetDto();
        to.setActive(from.getActive());
        to.setId(from.getWidgetId());
        to.setAppearance(from.getAppearance());
        to.setType(from.getType());
        to.setWidgetToken(from.getWidgetToken());
        to.setTopic(from.getTopic());
        to.setUserId(from.getUser().getUserId());
        to.setUserExternalId(from.getUser().getExternalId());
        return to;
    }

}
