package com.streamforge.service.exception;

public class WidgetExpection extends RuntimeException {

    public WidgetExpection(String message) {
        super(message);
    }

    public WidgetExpection(String message, Throwable throwable) {
        super(message, throwable);
    }

}
