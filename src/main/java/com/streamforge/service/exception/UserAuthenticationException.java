package com.streamforge.service.exception;

public class UserAuthenticationException extends RuntimeException {

    public UserAuthenticationException(String message) {
        super(message);
    }

    public UserAuthenticationException(String message, Throwable throwable) {
        super(message, throwable);
    }

}
