package com.streamforge.service.common.security.widget;

import com.streamforge.data.dto.twitch.ChannelDataNativeDto;
import com.streamforge.data.dto.widgets.WidgetDto;
import com.streamforge.data.dto.widgets.WidgetType;
import com.streamforge.data.entity.WebUser;
import com.streamforge.data.entity.Widget;
import com.streamforge.data.repository.WebUserRepository;
import com.streamforge.data.repository.WidgetRepository;
import com.streamforge.service.common.security.TokenService;
import com.streamforge.service.exception.UserAuthenticationException;
import com.streamforge.service.exception.WidgetExpection;
import com.streamforge.service.transformer.TransformerFactory;
import com.streamforge.service.twitch.TwitchUserDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class WidgetService {

    private static final Logger LOGGER = LoggerFactory.getLogger(WidgetService.class);

    @Autowired
    private WebUserRepository webUserRepository;

    @Autowired
    private WidgetRepository widgetRepository;

    @Autowired
    private TransformerFactory transformerFactory;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private TwitchUserDataService twitchUserDataService;

    @Transactional(propagation = Propagation.SUPPORTS)
    public WidgetDto createWidget(String externalToken, WidgetDto widgetDto, Long userId) {
        LOGGER.info("Creating widget for user {}", userId);

        WebUser webUser = webUserRepository.findById(userId)
                .orElseThrow(() -> new UserAuthenticationException("User not found"));

        Widget widget = new Widget();
        widget.setWidgetToken(tokenService.generateCustomToken(widget).substring(0, 15));
        widget.setActive(true);
        widget.setUser(webUser);
        widget.setAppearance(widgetDto.getAppearance());
        widget.setType(widgetDto.getType());
        widget.setTopic(getAppropriateTopic(widgetDto.getType(), externalToken, webUser.getExternalId(),
                webUser.getUsername()));

        widgetRepository.save(widget);

        return transformerFactory.getTransformer(Widget.class, WidgetDto.class).transform(widget);
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<WidgetDto> getWidgets(Long userId) {
        LOGGER.info("Getting widgets for user {}", userId);

        WebUser webUser = webUserRepository.findById(userId)
                .orElseThrow(() -> new UserAuthenticationException("User not found"));

        return webUser.getWidgets().stream()
                .map(widget -> transformerFactory.getTransformer(Widget.class, WidgetDto.class).transform(widget))
                .collect(Collectors.toList());
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public WidgetDto getWidget(Long userId, Long widgetId) {
        LOGGER.info("Getting widget {} for user {}", widgetId, userId);

        webUserRepository.findById(userId)
                .orElseThrow(() -> new UserAuthenticationException("User not found"));
        Widget widget = widgetRepository.findById(widgetId)
                .orElseThrow(() -> new WidgetExpection("Widget not found"));
        WidgetDto result = transformerFactory.getTransformer(Widget.class, WidgetDto.class)
                .transform(widget);
        result.setUserId(userId);

        return result;
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public WidgetDto getWidget(Long userId, String widgetToken) {
        LOGGER.info("Getting widget {} for user {}", widgetToken, userId);

        WebUser webUser = webUserRepository.findById(userId)
                .orElseThrow(() -> new UserAuthenticationException("User not found"));
        Widget widget = widgetRepository.findByToken(widgetToken)
                .orElseThrow(() -> new UserAuthenticationException("Widget not found"));
        WidgetDto result = transformerFactory.getTransformer(Widget.class, WidgetDto.class)
                .transform(widget);
        result.setUserId(webUser.getUserId());
        result.setUserExternalId(webUser.getExternalId());

        return result;
    }

    @Transactional(propagation = Propagation.SUPPORTS)
    public void removeWidget(Long widgetId) {
        LOGGER.info("Removing widget {}", widgetId);

        Widget widget = widgetRepository.findById(widgetId)
                .orElseThrow(() -> new UserAuthenticationException("Widget not found"));

        widgetRepository.delete(widget);
    }

    private String getAppropriateTopic(WidgetType widgetType, String token, Long userId, String userName) {
        ChannelDataNativeDto channelDataNativeDto = twitchUserDataService.getChannelDataByToken(token);
        switch (widgetType) {
            case BITS:
            case SUB:
                return String.format(widgetType.getTopic(), channelDataNativeDto.getId());
            case WHISPERS:
                return String.format(widgetType.getTopic(), userId);
        }
        return userName;
    }

}
