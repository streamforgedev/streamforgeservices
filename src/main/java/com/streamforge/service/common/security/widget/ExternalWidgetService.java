package com.streamforge.service.common.security.widget;

import com.streamforge.data.dto.widgets.PubSubSubscriptionDto;
import com.streamforge.data.dto.widgets.WidgetComplexDto;
import com.streamforge.data.dto.widgets.WidgetDto;
import com.streamforge.data.entity.ExternalSession;
import com.streamforge.data.entity.WebUser;
import com.streamforge.data.repository.WebUserRepository;
import com.streamforge.service.common.security.ExternalSessionService;
import com.streamforge.service.exception.UserAuthenticationException;
import com.streamforge.service.twitch.TwitchUserDataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;

@Service
public class ExternalWidgetService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExternalWidgetService.class);

    @Autowired
    private WebUserRepository webUserRepository;

    @Autowired
    private WidgetService widgetService;

    @Autowired
    private TwitchUserDataService twitchUserDataService;

    @Autowired
    private ExternalSessionService externalSessionService;

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public WidgetComplexDto getWidgetData(Long userId, String widgetToken) {
        LOGGER.info("Getting widget {} for user {}", widgetToken, userId);

        WidgetDto widgetDto = widgetService.getWidget(userId, widgetToken);
        ExternalSession externalSession = externalSessionService.getExternalSessionByUserId(userId)
                .orElseThrow(() -> new UserAuthenticationException("User is not authorized"));

        PubSubSubscriptionDto pubSubSubscriptionDto = twitchUserDataService
                .getPubSubSubscriptionBody(externalSession.getAccessToken(), Collections.singletonList(widgetDto.getTopic()));

        return new WidgetComplexDto(pubSubSubscriptionDto, widgetDto);
    }

}
