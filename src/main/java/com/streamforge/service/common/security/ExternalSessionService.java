package com.streamforge.service.common.security;

import com.streamforge.data.dto.TwitchTokenDto;
import com.streamforge.data.entity.ExternalSession;
import com.streamforge.data.repository.ExternalSessionRepository;
import com.streamforge.service.common.Constants;
import com.streamforge.service.exception.UserAuthenticationException;
import com.streamforge.service.twitch.TwitchTokenService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Optional;

@Service
public class ExternalSessionService {

    @Autowired
    private ExternalSessionRepository externalSessionRepository;

    @Autowired
    private TwitchTokenService twitchTokenService;

    @Autowired
    private TokenService tokenService;

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Long identifyWebUserId(String token) {
        String tokenBody = token.split(Constants.WHITESPACE)[1];

        return Optional.ofNullable(externalSessionRepository.findByToken(tokenBody))
                .map(ExternalSession::getUserId)
                .orElseThrow(() -> new UserAuthenticationException("External Session is not found"));
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public boolean validateExternalSession(String token) {
        return Optional.ofNullable(externalSessionRepository.findByToken(token)).isPresent();
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Optional<ExternalSession> getExternalSession(String token) {
        return Optional.ofNullable(externalSessionRepository.findByToken(token.split(" ")[1]));
    }

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public Optional<ExternalSession> getExternalSessionByUserId(Long userId) {
        return Optional.ofNullable(externalSessionRepository.findByUserId(userId));
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public String refreshServiceToken(String currentToken) {
        ExternalSession externalSession = externalSessionRepository.findByToken(currentToken);

        TwitchTokenDto refreshed = twitchTokenService.refreshCurrentSession(externalSession.getRefreshToken());
        externalSession.setAccessToken(refreshed.getAccessToken());
        externalSession.setRefreshToken(refreshed.getRefreshToken());
        Date expirationDate = new DateTime().plusSeconds(refreshed.getExpiration()).toDate();
        externalSession.setExpirationDate(expirationDate);

        String refreshedToken = tokenService.generateCustomToken(externalSession.getUser());
        externalSession.setServiceToken(refreshedToken);
        return refreshedToken;
    }

}
