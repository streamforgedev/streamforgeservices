package com.streamforge.service.common;

import com.streamforge.data.dto.UserDataDto;
import com.streamforge.data.dto.twitch.containers.TwitchUserListDto;
import com.streamforge.data.dto.twitch.TwitchUserDto;
import com.streamforge.data.entity.WebUser;
import com.streamforge.data.repository.WebUserRepository;
import com.streamforge.service.exception.UserAuthenticationException;
import com.streamforge.service.twitch.TwitchCallBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

import static com.streamforge.service.twitch.TwitchApi.Path.USER_PATH;

@Service
public class UserDataService {

    @Autowired
    private WebUserRepository webUserRepository;

    @Transactional(propagation = Propagation.REQUIRED)
    public UserDataDto processUserData(Long userId) {
        WebUser webUser = webUserRepository.findById(userId).orElseThrow(() -> new UserAuthenticationException("Invalid userId!"));
        return new UserDataDto()
                .setUserId(webUser.getUserId())
                .setExternalUserId(webUser.getExternalId())
                .setName(webUser.getUsername())
                .setName(webUser.getDisplayName());
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public TwitchUserDto getTwitchUserData(Long userId) {
        TwitchCallBuilder<Object, TwitchUserListDto> callBuilder = TwitchCallBuilder.instance(TwitchUserListDto.class);
        TwitchUserListDto result = callBuilder
                .setMethod(HttpMethod.GET)
                .setPath(USER_PATH)
                .addParam("id", userId)
                .call();
        return Objects.nonNull(result.getData()) && !result.getData().isEmpty()
                ? result.getData().get(0)
                : new TwitchUserDto();
    }

}
