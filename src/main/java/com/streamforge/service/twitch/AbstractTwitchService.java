package com.streamforge.service.twitch;

import org.springframework.beans.factory.annotation.Value;

public class AbstractTwitchService {

    @Value("${twitch.client.id}")
    protected String CLIENT_ID;

    @Value("${twitch.client.secret}")
    protected String CLIENT_SECRET;

    @Value("${twitch.api.streams.count}")
    protected int TOP_STREAMS_COUNT;

    protected static String PARAM_CLIENT_ID = "client_id";
    protected static String PARAM_CLIENT_SECRET = "client_secret";
    protected static String PARAM_GRANT_TYPE = "grant_type";
    protected static String PARAM_REFRESH_TOKEN = "refresh_token";

}
