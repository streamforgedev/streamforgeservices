package com.streamforge.service.twitch;

import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class TwitchCallBuilder<B, R> {

    private Class<R> responseClass;
    private Map<String, Object> queryParams;
    private HttpHeaders httpHeaders;
    private HttpMethod method;
    private String accessToken;
    private String path;
    private B body;
    private boolean useBearer = false;

    private TwitchCallBuilder(Class<R> responseClass) {
        this.responseClass = responseClass;
        httpHeaders = new HttpHeaders();
        queryParams = new HashMap<>();
    }

    public TwitchCallBuilder<B, R> setMethod(HttpMethod method) {
        this.method = method;
        return this;
    }

    public TwitchCallBuilder<B, R> setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        return this;
    }

    public TwitchCallBuilder<B, R> setBody(B body) {
        this.body = body;
        return this;
    }

    public TwitchCallBuilder<B, R> addHeader(String name, String value) {
        httpHeaders.add(name, value);
        return this;
    }

    public TwitchCallBuilder<B, R> addParam(String name, Object value) {
        queryParams.put(name, value);
        return this;
    }

    public TwitchCallBuilder<B, R> setPath(String path) {
        this.path = path;
        return this;
    }

    public TwitchCallBuilder<B, R> useBearer() {
        this.useBearer = true;
        return this;
    }

    public R call() {
        httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        httpHeaders.set(TwitchApi.AUTHORIZATION, !this.useBearer
                ? TwitchApi.wrapTwitchAccessToken(accessToken)
                : TwitchApi.wrapTwitchBearerToken(accessToken)
        );
        httpHeaders.set(TwitchApi.CLIENT_ID, TwitchApi.CLIENT_ID_VALUE);
        HttpEntity<B> entity = new HttpEntity<>(body, httpHeaders);

        if (!queryParams.isEmpty()) {
            path += prepareQueryParams();
        }

        RestTemplate restTemplate = new RestTemplate();
                ResponseEntity<R> response =
                restTemplate.exchange(path, method, entity, responseClass);

        return response.getBody();
    }

    private String prepareQueryParams() {
        return queryParams.entrySet().stream()
                .map(entry -> entry.getKey() + "=" + entry.getValue())
                .collect(Collectors.joining("&", "?", ""));
    }

    public static <B, R> TwitchCallBuilder<B, R> instance(Class<B> bodyClass, Class<R> responseClass) {
        return new TwitchCallBuilder<>(responseClass);
    }

    public static <R> TwitchCallBuilder<Object, R> instance(Class<R> responseClass) {
        return new TwitchCallBuilder<>(responseClass);
    }

}
