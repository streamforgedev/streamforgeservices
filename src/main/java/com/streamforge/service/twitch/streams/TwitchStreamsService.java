package com.streamforge.service.twitch.streams;

import com.streamforge.data.dto.twitch.containers.TwitchFollowersListDto;
import com.streamforge.data.dto.twitch.containers.TwitchStreamListDto;
import com.streamforge.data.dto.twitch.containers.TwitchUserListDto;
import com.streamforge.service.twitch.AbstractTwitchService;
import com.streamforge.service.twitch.TwitchApi;
import com.streamforge.service.twitch.TwitchCallBuilder;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

@Service
public class TwitchStreamsService extends AbstractTwitchService {

    public TwitchStreamListDto getStreamsData() {
        TwitchCallBuilder<Object, TwitchStreamListDto> callBuilder = TwitchCallBuilder.instance(TwitchStreamListDto.class);
        return callBuilder
                .setMethod(HttpMethod.GET)
                .setPath(TwitchApi.Path.GET_TWITCH_STREAMS)
                .addParam("first", TOP_STREAMS_COUNT)
                .call();
    }

    public TwitchStreamListDto getUserStream(Long userId, String token) {
        TwitchCallBuilder<Object, TwitchStreamListDto> callBuilder = TwitchCallBuilder.instance(TwitchStreamListDto.class);
        return callBuilder
                .setMethod(HttpMethod.GET)
                .useBearer()
                .setAccessToken(token)
                .setPath(TwitchApi.Path.GET_TWITCH_STREAMS)
                .addParam("user_id", userId)
                .call();
    }

    public TwitchFollowersListDto getFollowers(Long userId, String token) {
        TwitchCallBuilder<Object, TwitchFollowersListDto> callBuilder = TwitchCallBuilder.instance(TwitchFollowersListDto.class);
        return callBuilder
                .setMethod(HttpMethod.GET)
                .useBearer()
                .setAccessToken(token)
                .setPath(TwitchApi.Path.FOLLOWERS_PATH)
                .addParam("to_id", userId)
                .call();
    }

    public TwitchFollowersListDto getFollowed(Long userId, String token) {
        TwitchCallBuilder<Object, TwitchFollowersListDto> callBuilder = TwitchCallBuilder.instance(TwitchFollowersListDto.class);
        return callBuilder
                .setMethod(HttpMethod.GET)
                .useBearer()
                .setAccessToken(token)
                .setPath(TwitchApi.Path.FOLLOWERS_PATH)
                .addParam("from_id", userId)
                .call();
    }

}
