package com.streamforge.service.twitch;

import com.streamforge.data.dto.twitch.ChannelDataNativeDto;
import com.streamforge.data.dto.widgets.PubSubPayloadDto;
import com.streamforge.data.dto.twitch.UserDataNativeDto;
import com.streamforge.data.dto.widgets.PubSubSubscriptionDto;
import com.streamforge.data.dto.twitch.containers.TwitchBitsListDto;
import com.streamforge.data.dto.twitch.containers.TwitchComplexDto;
import com.streamforge.service.twitch.streams.TwitchStreamsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TwitchUserDataService extends AbstractTwitchService {

    @Autowired
    private TwitchStreamsService twitchStreamsService;

    public UserDataNativeDto getUserDataByToken(String accessToken) {
        TwitchCallBuilder<Object, UserDataNativeDto> callBuilder = TwitchCallBuilder.instance(UserDataNativeDto.class);
        return callBuilder
                .setMethod(HttpMethod.GET)
                .setAccessToken(accessToken)
                .setPath(TwitchApi.Path.GET_USER_BY_ACCESS_TOKEN)
                .call();
    }

    public ChannelDataNativeDto getChannelDataByToken(String accessToken) {
        TwitchCallBuilder<Object, ChannelDataNativeDto> callBuilder = TwitchCallBuilder.instance(ChannelDataNativeDto.class);
        return callBuilder
                .setMethod(HttpMethod.GET)
                .setAccessToken(accessToken)
                .setPath(TwitchApi.Path.GET_CHANNEL_BY_ACCESS_TOKEN)
                .call();
    }

    public TwitchBitsListDto getBitsLeaderboard(String accessToken) {
        TwitchCallBuilder<Object, TwitchBitsListDto> callBuilder = TwitchCallBuilder.instance(TwitchBitsListDto.class);
        return callBuilder
                .setMethod(HttpMethod.GET)
                .useBearer()
                .setAccessToken(accessToken)
                .setPath(TwitchApi.Path.BITS_LEADERBOARD_PATH)
                .addParam("count", 100)
                .call();
    }

    public TwitchComplexDto getFull(Long userId, String accessToken) {
        TwitchComplexDto twitchComplexDto = new TwitchComplexDto();
        twitchComplexDto.setBits(this.getBitsLeaderboard(accessToken));
        twitchComplexDto.setFollowed(twitchStreamsService.getFollowed(userId, accessToken));
        twitchComplexDto.setFollowers(twitchStreamsService.getFollowers(userId, accessToken));
        twitchComplexDto.setStream(twitchStreamsService.getUserStream(userId, accessToken));
        return twitchComplexDto;
    }

    public PubSubSubscriptionDto getPubSubSubscriptionBody(String token, List<String> topics) {
        PubSubPayloadDto pubSubPayloadDto = new PubSubPayloadDto();
        pubSubPayloadDto.setAuth_token(token);
        pubSubPayloadDto.setTopics(topics);

        PubSubSubscriptionDto pubSubSubscriptionDto = new PubSubSubscriptionDto();
        pubSubSubscriptionDto.setType("LISTEN");
        pubSubSubscriptionDto.setData(pubSubPayloadDto);

        return pubSubSubscriptionDto;
    }

}
