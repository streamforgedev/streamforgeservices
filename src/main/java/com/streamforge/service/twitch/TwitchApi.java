package com.streamforge.service.twitch;

public class TwitchApi {

    // Common values
    public static final String AUTHORIZATION = "Authorization";
    public static final String CLIENT_ID = "Client-ID";
    public static final String CLIENT_ID_VALUE = "55upy7njxjo97rx68f7iv8iy6ezc2s";

    // Utilities
    public static String wrapTwitchAccessToken(String token) {
        return "OAuth " + token;
    }

    public static String wrapTwitchBearerToken(String token) {
        return "Bearer " + token;
    }

    // Requests' pafs
    public static class Path {
        public static final String BASE_TWITCH_API_PATH = "https://api.twitch.tv";
        public static final String NEW_TWITCH_API_PATH = "/helix";
        public static final String V5_TWITCH_API_PATH = "/kraken";

        public static final String GET_TWITCH_STREAMS = BASE_TWITCH_API_PATH + NEW_TWITCH_API_PATH + "/streams";
        public static final String GET_USER_BY_ACCESS_TOKEN = BASE_TWITCH_API_PATH + V5_TWITCH_API_PATH + "/user";
        public static final String GET_CHANNEL_BY_ACCESS_TOKEN = BASE_TWITCH_API_PATH + V5_TWITCH_API_PATH + "/channel";
        public static final String REFRESH_ACCESS_TOKEN = "https://id.twitch.tv/oauth2/token";

        public static final String USER_PATH = BASE_TWITCH_API_PATH + NEW_TWITCH_API_PATH + "/users";
        public static final String BITS_LEADERBOARD_PATH = BASE_TWITCH_API_PATH + NEW_TWITCH_API_PATH + "/bits/leaderboard";
        public static final String FOLLOWERS_PATH = USER_PATH + "/follows";
    }
}
