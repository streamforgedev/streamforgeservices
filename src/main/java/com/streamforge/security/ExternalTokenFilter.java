package com.streamforge.security;

import com.streamforge.service.common.Constants;
import com.streamforge.service.common.security.ExternalSessionService;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ExternalTokenFilter extends OncePerRequestFilter {

    private ExternalSessionService externalSessionService;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest,
                                    HttpServletResponse httpServletResponse,
                                    FilterChain filterChain) throws ServletException, IOException {

        if (externalSessionService == null) {
            ServletContext servletContext = httpServletRequest.getServletContext();
            WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
            externalSessionService = webApplicationContext.getBean(ExternalSessionService.class);
        }

        String token = httpServletRequest.getHeader("Authorization");
        if (token == null) {
            httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        } else {
            if (token.startsWith(Constants.TOKEN_CUSTOM)) {
                String tokenValue = token.split(Constants.WHITESPACE)[1];
                if (externalSessionService.validateExternalSession(tokenValue)) {
                    filterChain.doFilter(httpServletRequest, httpServletResponse);
                } else {
                    httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                }
            }
        }
    }

}
